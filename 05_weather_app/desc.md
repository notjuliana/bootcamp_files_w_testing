React Project: Weather App
Create a weather forecast app according to the following requirements:


1. it has to display the local time and temperature on page load, based on the location of the device.
2. it has to have an input that can be used to search for the weather by city name.
3. you need to be able to see the weather-forecast  for at least one day.
4. it needs to have autosuggestions for the city while typing (look into google autocomplete api)
5. for this exercise you will need to use external apis.
6. You need to have this in github from the very beginning and keep it up to date so that we can check your progress from your repo.


You will need to use OpenWeatherMap and Google Autocomplete and learn how to use them by reading the documentation of the APIs.

You can do other google search but not look for a tutorial on how to make a weather app as this would defeat the purpose of the exercise.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_A3EAE66FDE56DF5B275A6948D62DF2E41468129544097C7E8214E980C16FF74A_1506415548302_Screen+Shot+2017-09-26+at+10.27.17.png)


see example below to get an idea.
http://jqueryweatherapp.herokuapp.com/ 

***Your solution goes to the current folder***