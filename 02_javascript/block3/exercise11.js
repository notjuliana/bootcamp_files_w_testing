var lowerCaseLetters = (str)=> {
    let arr =[]
    str.split('').map(ele => {
		if(isNaN(Number(ele))) {
			 ele == ele.toUpperCase()  ? arr.push(' '+ele) : arr.push(ele)
		}
	})
    return arr.join('').toLowerCase().trim()
}

module.exports = {
	lowerCaseLetters
}