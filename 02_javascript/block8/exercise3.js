var tally   = arr =>{
    var obj = {}
    arr.map( ele => obj[ele] = obj[ele]+1 || 1 )
    return obj
}
module.exports = {
    tally
}