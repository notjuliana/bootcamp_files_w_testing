var tally   = (arr, action) =>{
    var obj = {}
    arr.map( ele => obj[ele] = obj[ele]+1 || 1 )
    if (action == 'obj' || !action) {
        return obj
    }else{
        var arr = []
        for (var key in obj){
            arr.push([key, obj[key]])
        }
        return arr
    }
}
module.exports = {
    tally
}