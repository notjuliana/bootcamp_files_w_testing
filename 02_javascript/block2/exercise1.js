    //write a funtion called assigner which takes 2 arguments, an array and an index.
    // it then takes from the array the value at the index passed as argumen and assign it as the only value to an array called arr2.
    //Then return arr2.
    var arr   = ['milk','cheese','car','lime'];
    var index = 2; 
    // assigner (arr, 2)  should return ['car']


    var assigner = function(arr, index){
        var arr2 =[arr[index]]
        return arr2
    }

    module.exports = {
        arr, assigner, index
    }