var shuffle  = a => {
	a.forEach( (_, i) => {
		var newPos = Math.floor(Math.random() * (i + 1)), temp= a[newPos]
		a[newPos] = a[i], a[i] = temp
	})
	return a 
}
module.exports = {
    shuffle
}