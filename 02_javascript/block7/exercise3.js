class BankAccount {
	constructor(amount){
		this.amount = amount  || 0;
	}
    deposit(val) { this.amount +=val  }
    withdraw(val) { this.amount -= val }
    balance(){return this.amount}
}
module.exports = {
    BankAccount
}
