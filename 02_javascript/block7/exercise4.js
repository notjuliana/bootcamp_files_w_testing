var Universe = function(init, which) {
	var self  = this
        this.matter = {
            total: which == 'matter' ? init : 0,
            destroy:function(val){
                this.total -= val
                self.energy.total += val
            },
            create:function(val){
                this.total += val
                self.energy.total -= val
            }
        }
        this.energy = {
            total: which == 'energy' ? init : 0,
            destroy:function(val){
                this.total -= val
                self.matter.total += val
            },
            create:function(val){
                this.total += val
                self.matter.total -= val
            }
        }
}

module.exports = {
    Universe
}