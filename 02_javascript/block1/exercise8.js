
//knowing that the minimum age for driving a 50cc scooter is 15 ask yourself how old you are with a prompt
// store the age in a variable
// store 15 in a variale called minAge
// then return true if you are old enough to drive a scooter and false if you are not.

var age     = 21;
var minAge  = 15;

var checkAge = function (age, minAge){
    return age >= minAge;
}

module.exports = {
    checkAge,
    age,
    minAge
}