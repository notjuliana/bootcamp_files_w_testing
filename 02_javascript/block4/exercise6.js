var numberConverter = arr =>{
    let numbers = [], unconvertable = [];
    arr.map( ele =>{
        if(typeof ele != 'number'){
            !isNaN(ele) ? numbers.push(ele) : unconvertable.push(ele);
        }
    })
    if(numbers.length > 1) return `${numbers.length} were converted to numbers  ${numbers} ${unconvertable.length} couldn't be converted`
    return 'no need for conversion'
}

module.exports ={
    numberConverter
}