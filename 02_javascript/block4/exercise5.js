var howManyCaps = str =>{
    let capitals = [],
        isCapital = ele => ele == ele.toUpperCase() && ele != ' '
    str.split('').map(ele =>{
         isCapital(ele) ? capitals.push(ele) : null
    });
    return `There are ${capitals.length} capitals and these are ${capitals}`
}

module.exports = {
    howManyCaps
}