var sort = obj => {
    var arr = [];
    for (var key in obj) {
        arr.push([key, obj[key]]);
    }
    arr.sort((a, b)=>  a[1] - b[1]);
    var newObj = {}
    arr.map(ele => newObj[ele[0]] = ele[1])
    return newObj;
}

module.exports ={
    sort
}
