var model = (action, obj, schema) =>{
    var DB = []
    if (!action || action != 'add') throw Error ("missing action argument or wrong provided")
    var newObj = {}
    for(var key in obj){
        key in schema && typeof obj[key] == schema[key].type ? newObj[key] = obj[key] :null
    }
    for (var key in schema){
        newObj[key] == undefined && schema[key].default != undefined ? newObj[key] = schema[key].default : null
    }
    DB.push(newObj)
    return DB;
}

module.exports = {
    model
}