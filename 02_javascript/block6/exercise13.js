var bankAccount  =  {
    total: 0,
    withdraw : function(num){ this.total-= num  },
    deposit  : function(num){ this.total += num },
    balance  : function()   { return this.total    }
}
module.exports = {
    bankAccount
}