EXERCISE 1:
    //write a funtion called assigner which takes 2 arguments, an array and an index.
    // it then takes from the array the value at the index passed as argumen and assign it as the only value to an array called arr2.
    //Then return arr2.
    var arr   = ['milk','cheese','car','lime'];
    var index = 2; 
    // assigner (arr, 2)  should return ['car']

***Your solution goes after this line***






EXERCISE 2:
    //write a function called takeAll which takes an array as argument, it then returns a new array which has all values of he orignal array but in reverse order.
    //Please convert your array to a string before returning it.
    //=========================Example=========================
    //BAD:  return myNewArray
    //GOOD: return String(myNewArray)
    var arr = ['milk','cheese','car','lime']
    //example takeAll(arr) returns ["lime", "car", "cheese", "milk"]
***Your solution goes after this line***






EXERCISE 3:
    //create a function called swap which takes two arrays as the first two arguments and a position as the third
    //It then returns the two arrays swapping the value at the position passed as argument.
    // return both arrays inside another array (as a string) see example below:
    //EXAMPLE
    //========GOOD=========
    //function swap(arr, arr2,pos){
       // ...your code gors here
        //return String([arr, arr2])
    //}
    //=========BAD=========
        //function swap(arr, arr2,pos){
       // ...your code gors here
        //return arr, arr2
    //}
    var arr  =  ['banana','apple','orange'], 
        arr2 =  ['tv','dvd-player','playstation'], 
        pos  =  2;
    
    //EXAMPLE
    swap(arr, arr2, pos)
    // returns (2) [Array(3), Array(3)]
    //["banana", "apple", "playstation"]
    //["tv", "dvd-player", "orange"]

***Your solution goes after this line***





    

EXERCISE 4:
    // define two variables: an array(you can use the example) and another one called position(a number)
    //create a function called splicer which removes an element from an array and returns the array with the element removed.
    //this function takes two arguments"
        //the array
        //the position of the element in the array to be removed.
    //Once again please stringify the array before returning it.
    //===================example===================
    // return String(arr)
    
    var arr = ['cheese','salame','bread','water','pizza']
    
    //expected output:arr = ['cheese','salame','water','pizza']

***Your solution goes after this line***






EXERCISE 5:
    //write a function called removeFirstAndLast
    //declare a variable called arr =(use example array)
    //it takes an array as an argument and returs an array of which the first and last elements have be removed.

    var arr = ['car','soap','banana','tv','toothbrush']
    //expected output arr = ['soap','banana','tv']

        //Once again please stringify the array before returning it.
    //===================example===================
    // return String(arr)

***Your solution goes after this line***






EXERCISE 6:
    //declare a variable called arr (use example array)
    //write a function called removeAll 
    //it takes an array as an argument and returs an array of which the all elements have be removed.
    var arr =[1,2,3,4,5,6,7,8,9,0,3,4,523,44,3454]
    //expected output arr =[]
            //Once again please stringify the array before returning it.
    //===================example===================
    // return String(arr)

***Your solution goes after this line***






EXERCISE 7:
    // write a function called pusher.
    // it takes one argument an array.
    //using push take all elements from the array  and push them to a new variable called arr2
    //then return  arr2
    
    var arr = ["one","two","three","four"]
    //expected output  ["one","two","three","four"]
        //expected output arr =[]
            //Once again please stringify the array before returning it.
    //===================example===================
    // return String(arr)

***Your solution goes after this line***






EXERCISE 8:
    // create a function called take_and_push which takes 3 arguments, an array and two numbers.
    // the second and third arguments are indexes
    // you need to take the elements in the array that are at the given index and push them to a new array.
    //then return the new array.

    var arr = ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office']

    //expected output arr =[]
            //Once again please stringify the array before returning it.
    //===================example===================
    // return String(arr)
***Your solution goes after this line***






EXERCISE 9:
    //create a function called concatenator which takes an array as argument
    // and then using Array.concat it returns a new array which s a clone of the origiinal array.

    var arr = ['Breaking bad','WestWorld','Psych','Games of Thrones','Gotham','Spartacus','Dexter','Office']

    //Once again please stringify the array before returning it.
    //===================example===================
    // return String(arr)
***Your solution goes after this line***








EXERCISE 10:
    //create a function calld findPosition which takes two arguments, an arrat and an element and it return the index of the given element.
    var arr = ['glass','car','watch','sofa','macbook']

***Your solution goes after this line***






EXERCISE 11:
    //create a function called isThere which takes an array as first argument and a string as second, it needs to check if the string is inside the array and returns true if it is and false if it's not.

    
    //green, red, pink,
    
    var arr = ['green','red','black','blue','brown','yellow','purple']
    //expected output
    
    //true
    //true
    //false

***Your solution goes after this line***






EXERCISE 12:

    //now do the same exercise using the method includes only this time you should console.log false if the color is there and true if it's not!
    var arr = ['green','red','black','blue','brown','yellow','purple']
    //green, red, pink,
    
    //expected output
    //false
    //false
    //true

***Your solution goes after this line***






EXERCISE 13:
    //write a function called characterRemover which takes two arguments, a string and a carachter to be removed and returns the string free of the unwanted character.

    var str = 'I,Really,Like,Pizza'


***Your solution goes after this line***





EXERCISE 14
   // write a function called isArrayFunc which takes an argument and returns true if it's an array and false if it's anything else.
    var one   = {name:'antonello'}
    var two   = ['name', 'antonello']
    var three = [[],[],{},"antonello",3,function(){}]
    
    //expected output 
    false
    true
    true

***Your solution goes after this line***





